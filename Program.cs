﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace comp6211arrays
{
	class Program
	{
		static int[] Arr0 = new int[10];
		static string[] Arr1 = new string[10];
		
		static int[] Arr0cpy = new int[10];
		static string[] Arr1cpy = new string[10];
		
		static void Main(string[] args)
		{
			// Populate these arrays with dummy data and display them on screen.

			Random populateArr = new Random();

			for (int i = 0; i <= Arr0.Length - 1; i++)
			{
				Arr0[i] = populateArr.Next(1,100);
				Arr1[i] = "John" + Convert.ToString(populateArr.Next(100,200));
			}

			Console.WriteLine("--Array 0:");
			
			foreach (int index in Arr0)
			{
				Console.Write("{0}, ", index);
			}

			Console.WriteLine("\n--Array 1:");

			foreach (string index in Arr1)
			{
				Console.Write("{0}, ", index);
			}

			Console.WriteLine("\n--Choose what to do with the arrays:");
			Console.WriteLine("--getLength (1), copyArrays(2) getType (3), getValue5 (4), searchValue (5)");
			Console.WriteLine("--reverseArray (6), changeLocation5 (7), sortArrays (8):");

			while (true)
			{
				int switchChoice = int.Parse(Console.ReadLine());
				switch(switchChoice)
				{
					case 1:
						getLength();
						break;
					case 2:
						copyArrays();
						Console.WriteLine("Arrays have been copied");
						break;
					case 3:
						getType();
						break;
					case 4:
						getValue5();
						break;
					case 5:
						searchValue();
						break;
					case 6:
						reverseArray();
						break;
					case 7:
						changeLocation5();
						break;
					case 8:
						sortArrays();
						break;
				}
			}
			
		}

		public static void getLength()
		{
			// Get the length-of both arrays

			Console.WriteLine("Array 0 Length: {0}, Array 1 Length: {1}", Arr0.Length, Arr1.Length);
		}

		public static void copyArrays()
		{
			// Copy these arrays to new arrays

			for (int i = 0; i <= Arr0.Length; i++)
			{
				Array.Copy(Arr0, Arr0cpy, i);
				Array.Copy(Arr1, Arr1cpy, i);
			}
		}

		public static void getType()
		{
			// Get the type of each array

			Type Arr0Type = Arr0.GetType();
			Type Arr0cpyType = Arr0cpy.GetType();
			Type Arr1Type = Arr1.GetType();
			Type Arr1cpyType = Arr1cpy.GetType();

			Console.WriteLine("--Array 0 Type: {0}, Array 0 Copy Type: {1}", Arr0Type, Arr0cpyType);
			Console.WriteLine("--Array 1 Type: {0}, Array 1 Copy Type: {1}", Arr1Type, Arr1cpyType);
		}

		public static void getValue5()
		{
			// Get the values of each array at index 5

			Console.WriteLine("--Index 5 of all arrays:");
			Console.WriteLine("Array 0: {0}", Arr0[5]);
			Console.WriteLine("Array 1: {0}", Arr1[5]);
			Console.WriteLine("Array 0 Copy: {0}", Arr0cpy[5]);
			Console.WriteLine("Array 1 Copy: {0}", Arr1cpy[5]);
		}

		public static void searchValue()
		{
			// Search for a specific value in an array and display the output

			Console.WriteLine("--Choose an array to get a value from");
			Console.WriteLine("--1 (Array 0), 2 (Array 0 Copy), 3 (Array 1), 4 (Array 1 Copy), and 5 (ALL):");
			int arrayChoice = int.Parse(Console.ReadLine());
			
			Console.WriteLine("--Choose what value to search for:");
			var search = Console.ReadLine();
			int arrayIndex;

			Console.WriteLine("--Search Results:");

			if (arrayChoice == 1 || arrayChoice == 5)
			{
				if(int.TryParse(search, out int searchParsed) == true)
				{
					arrayIndex = Array.IndexOf(Arr0, searchParsed);
				}
				else
				{
					arrayIndex = -1;
				}
				
				if (arrayIndex != -1) 
				{
					Console.WriteLine("Found '{0}' in Array 0", Arr0[arrayIndex]);
				}
				else
				{
					Console.WriteLine("Value '{0}' not found in Array 0", search);
				}
			}
		
			if (arrayChoice == 2 || arrayChoice == 5)
			{
				if(int.TryParse(search, out int searchParsed) == true)
				{
					arrayIndex = Array.IndexOf(Arr0cpy, searchParsed);
				}
				else
				{
					arrayIndex = -1;
				}

				Console.WriteLine(arrayIndex);
				if (arrayIndex != -1) 
				{
					Console.WriteLine("Found '{0}' in Array 0 Copy", Arr0cpy[arrayIndex]);
				}
				else
				{
					Console.WriteLine("Value '{0}' not found in Array 0 Copy", search);
				}
			}
			
			if (arrayChoice == 3 || arrayChoice == 5)
			{
				arrayIndex = Array.IndexOf(Arr1, search);
				
				if (arrayIndex != -1) 
				{
					Console.WriteLine("Found '{0}' in Array 1", Arr1[arrayIndex]);
				}
				else
				{
					Console.WriteLine("Value '{0}' not found in Array 1", search);
				}
			}

			if (arrayChoice == 4 || arrayChoice == 5)
			{
				arrayIndex = Array.IndexOf(Arr1cpy, search);
				
				if (arrayIndex != -1) 
				{
					Console.WriteLine("Found '{0}' in Array 1 Copy", Arr1cpy[arrayIndex]);
				}
				else
				{
					Console.WriteLine("Value '{0}' not found in Array 1 Copy", search);
				}
			}
		}

		public static void reverseArray()
		{
			// Reverse the entire array

			Console.WriteLine("--Choose an array to reverse");
			Console.WriteLine("--1 (Array 0), 2 (Array 0 Copy), 3 (Array 1), 4 (Array 1 Copy), and 5 (ALL):");
			int arrayChoice = int.Parse(Console.ReadLine());

			Console.WriteLine("--Array Reversals:");

			if (arrayChoice == 1 || arrayChoice == 5)
			{
				Array.Reverse(Arr0);
				Console.Write("Array 0 Reversed: ");

				foreach (int index in Arr0)
				{
					Console.Write("{0}, ", index);
				}
				
				Console.Write('\n');
			}

			if (arrayChoice == 2 || arrayChoice == 5)
			{
				Array.Reverse(Arr0cpy);
				Console.Write("Array 0 Copy Reversed: ");
				
				foreach (int index in Arr0cpy)
				{
					Console.Write("{0}, ", index);
				}

				Console.Write('\n');
			}

			if (arrayChoice == 3 || arrayChoice == 5)
			{
				Array.Reverse(Arr1);
				Console.Write("Array 1 Reversed: ");
				
				foreach (string index in Arr1)
				{
					Console.Write("{0}, ", index);
				}

				Console.Write('\n');
			}

			if (arrayChoice == 4 || arrayChoice == 5)
			{
				Array.Reverse(Arr1cpy);
				Console.Write("Array 1 Copy Reversed: ");
				
				foreach (string index in Arr1cpy)
				{
					Console.Write("{0}, ", index);
				}

				Console.Write('\n');
			}
		}

		public static void changeLocation5()
		{
			// Change the values of both arrays at location 5

			Arr0[4] = 1234;
			Arr1[4] = "Smith";
		}

		public static void sortArrays()
		{
			// Sort the arrays in ascending and descending orders

			Console.WriteLine("--Choose an array to sort");
			Console.WriteLine("--1 (Array 0), 2 (Array 0 Copy), 3 (Array 1), 4 (Array 1 Copy), and 5 (ALL):");
			int arrayChoice = int.Parse(Console.ReadLine());

			Console.WriteLine("--Choose either Ascending (1) or Descending (2):");
			int sortingChoice = int.Parse(Console.ReadLine());

			if (arrayChoice == 1 || arrayChoice == 5)
			{
				if (sortingChoice == 1)
				{
					Array.Sort(Arr0);
					Console.Write("Array 0 Sorted Ascendingly: ");
				}
				else if (sortingChoice == 2)
				{
					Array.Sort(Arr0);
					Array.Reverse(Arr0);
					Console.Write("Array 0 Sorted Descendingly: ");
				}

				foreach (int index in Arr0)
				{
					Console.Write("{0}, ", index);
				}
				
				Console.Write('\n');
			}

			if (arrayChoice == 2 || arrayChoice == 5)
			{
				if (sortingChoice == 1)
				{
					Array.Sort(Arr0cpy);
					Console.Write("Array 0 Copy Sorted Ascendingly: ");
				}
				else if (sortingChoice == 2)
				{
					Array.Sort(Arr0cpy);
					Array.Reverse(Arr0cpy);
					Console.Write("Array 0 Copy Sorted Descendingly: ");
				}
				
				foreach (int index in Arr0cpy)
				{
					Console.Write("{0}, ", index);
				}

				Console.Write('\n');
			}

			if (arrayChoice == 3 || arrayChoice == 5)
			{
				if (sortingChoice == 1)
				{
					Array.Sort(Arr1);
					Console.Write("Array 1 Sorted Ascendingly: ");
				}
				else if (sortingChoice == 2)
				{
					Array.Sort(Arr1);
					Array.Reverse(Arr1);
					Console.Write("Array 1 Sorted Descendingly: ");
				}
				
				foreach (string index in Arr1)
				{
					Console.Write("{0}, ", index);
				}

				Console.Write('\n');
			}

			if (arrayChoice == 4 || arrayChoice == 5)
			{
				if (sortingChoice == 1)
				{
					Array.Sort(Arr1cpy);
					Console.Write("Array 1 Copy Sorted Ascendingly: ");
				}
				else if (sortingChoice == 2)
				{
					Array.Sort(Arr1cpy);
					Array.Reverse(Arr1cpy);
					Console.Write("Array 1 Copy Sorted Descendingly: ");
				}

				foreach (string index in Arr1cpy)
				{
					Console.Write("{0}, ", index);
				}

				Console.Write('\n');
			}
		}
	}
}
